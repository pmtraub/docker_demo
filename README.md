# Intro to Docker Workshop

The goal of this workshop is quickly create, run, and destroy a Docker container on your workstation. The initial setup to get Docker running will vary depending on which platform you're on. 

### Prerequisites

To do this workshop, you must have Docker installed for your platform. Please follow the platform specific installation directions here:

Mac - https://docs.docker.com/mac/step_one/

Win - https://docs.docker.com/windows/step_one/

Linux - https://docs.docker.com/linux/step_one/

### Setup

Get the repository

    git clone git@bitbucket.org:pmtraub/docker_demo.git

then cd into the `docker_demo` directory and `cat Dockerfile`. You will see two lines:

    FROM php:5.6-apache
    COPY ./index.php /var/www/html/
    
The first line pulls the PHP with Apache image from Docker Hub. You can find that image here, along with more instructions on how to use it:  https://hub.docker.com/_/php/

The second line copies the `index.php` file in your `docker_demo` directory into the `/var/www/html` directory of the container you are about to build.

Start the boot2docker Linux image in VirtualBox. This is a special optimized Linux distro installed by Docker to enable the docker engine daemon to run in its native environment when running on non-Linux base OSs. 

    docker-machine start

(once the machine is started, you can ssh into it if interested with `docker-machine ssh default`. `ps -aux|grep docker` to see all the running Docker processes)

### Building and running the App

From inside `docker_demo` (on your base OS, not the Docker Machine instance), run:

    docker build -t my-php-app .

Run the app:

    docker run --rm -p 80:80 --name my-php-container my-php-app

The flags are as follows:

    --rm   = Automatically remove the container when it exits
    -p     = Publish a container's port(s) to the host (using format host-port:container-port)
    --name = Assign a name to the container

The last argument is the name of the image you want to run the container from.
    
In OS X and Windows, run `docker-machine ls` to see some basic info the running Docker Machine instance. Grab the IP address that is attached to the Boot2Docker VM and plug it into your browser. You should get a PHP generated info page showing a large quantity of system information. When running Docker in Linux, this step is unnecessary as you're not running the Boot2Docker VM - the Docker engine is just talking straight to your OS.

In another terminal window, lets see info on your running container(s) (you may need to run `eval $(docker-machine env)` again in the new terminal first):

    docker ps

Now stop the container with `docker stop my-php-container`. Run `docker ps` again. Where did it go? It was auto-deleted because of the `--rm` flag. However, the image file you built is still there when you run: `docker images`.

Let's run a second container based on that same image. We'll give it a different name, and we won't set this one to auto-delete when it exits.

    docker run -d -it -p 8080:80 --name my-php-container2 my-php-app

Notice we've removed the `--rm` flag and added `-d`. `-d` detaches the container and runs it in the background, essentially daemonizing it. 

Point your browser to the same IP as before, but now to port 8080.

You can also see the log output from the container with `docker logs my-php-container2`

Stop the container with `docker stop my-php-container2`

Now, unlike the with the first container, this container is still there waiting to be run again. You can see it with `docker ps -a`. The `-a` flag shows all containers, both running and stopped.

To start it running again, you don't pass the run command, as that is just for first-time use. Now you can just start it with `docker start my-php-container2`, and it will instantly run again with all the parameters that were passed to it the first time.

### Cleaning up

Stop the container with `docker stop my-php-container2`

Since we don't need this container anymore, we can remove it with `docker rm -v my-php-container2`. **Caution**: The `-v` flag will remove any volumes associated with the container. In production use it is common to want volumes to remain intact so that you can have data persistence between container instances (with a database for example).

We also don't need our image anymore, and can remove that with `docker rmi my-php-app`
    